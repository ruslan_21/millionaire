<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "answer".
 *
 * @property integer $id
 * @property string $text_answer
 * @property integer $question_id
 *
 * @property Question $question
 * @property Question[] $questions
 */
class Answer extends \yii\db\ActiveRecord
{

    public static $division = [
            'Answer A',
            'Answer B',
            'Answer C',
            'Answer D'
        ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text_answer'], 'required'],
            [['question_id'], 'integer'],
            [['text_answer'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text_answer' => 'Text answer',
            'question_id' => 'Question ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['id_answer_true' => 'id']);
    }
}
