<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have correctly answered all the questions</p>

        <p><?= Html::a('Start new Game', ['game/start'], ['class' => 'btn btn-lg btn-success']) ?></p>
    </div>
</div>
