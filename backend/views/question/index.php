<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.01.16
 * Time: 0:01
 */

/* @var $dataProvider*/
/* @var $this yii\web\View */
use yii\bootstrap\Html;
use yii\grid\GridView;
$this->title = 'Questions';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= $this->title ?></h1>

<p>
<?= Html::a('Create question', ['create'], ['class' => 'btn btn-primary btn big']) ?>
</p>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'text_question',
        'level_id',
        [
            'class' => \yii\grid\ActionColumn::className()
        ]
    ]
]); ?>
