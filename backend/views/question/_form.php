<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.01.16
 * Time: 15:50
 */

/* @var $question common\models\Question*/
/* @var $answers \common\models\Answer[] */
/* @var $this yii\web\View */

use common\models\Answer;
use yii\helpers\Html;

?>
<?php $form = \yii\widgets\ActiveForm::begin() ?>
<?= $form->field($question, 'text_question')->textarea(); ?>
<?php foreach ($answers as $index => $answer): ?>
    <?= $form->field($answer, "[$index]text_answer")
        ->label(Answer::$division[$index] . ':') ?>
<?php endforeach ?>

<?= $form->field($question, 'id_answer_true')->dropDownList(Answer::$division) ?>
<?= $form->field($question, 'level_id')->dropDownList([1 => 1, 2 => 2, 3 => 3]) ?>

    <div class="form-group">
        <?= Html::submitButton($question->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-primary']) ?>
    </div>
<?php $form->end(); ?>