<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.01.16
 * Time: 15:33
 */

/* @var $question */
/* @var $answers \common\models\Answer[] */
/* @var $this yii\web\View */


use yii\helpers\Html;
use common\models\Answer;

$this->title = 'Create question';
$this->params['breadcrumbs'][] = ['label' => 'Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= $this->title ?></h1>

<?= $this->render('_form', ['question' => $question, 'answers' => $answers]); ?>


