<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "question_level".
 *
 * @property integer $question_number_id
 * @property integer $level
 * @property integer $sum
 */
class QuestionLevel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level', 'sum'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'question_number_id' => 'Question Number ID',
            'level' => 'Level',
            'sum' => 'Sum',
        ];
    }
}
