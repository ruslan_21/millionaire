<?php

use yii\db\Schema;
use yii\db\Migration;

use common\models\Question;
use common\models\Answer;

class m160205_220921_questions_for_testing_system extends Migration
{
    public function up()
    {
        for ($level = 1; $level <= 3; $level++) {
            for ($numberOfQuestion = 1; $numberOfQuestion <= 20;
                 $numberOfQuestion++
            ) {
                $question = new Question();
                $question->text_question =
                    "Вопрос $numberOfQuestion уровня $level";
                $question->level_id = $level;
                $question->save(false);

                $answers = [];
                for ($i=1; $i <= count(Answer::$division); $i++) {
                    $answer = new Answer();
                    $answer->text_answer =
                        'Ответ ' . $i . ' на вопрос ' . $numberOfQuestion .
                        ' уровня ' . $level;
                    $answer->question_id = $question->id;
                    $answer->save(false);
                }

                $question->id_answer_true = $answer->id;
                $question->save(false);

            }
        }
    }

    public function down()
    {
        /* @var Question[] $questions */
        $questions = Question::find()->all();

        foreach ($questions as $question) {
            $question->id_answer_true = null;
            $question->save(false);
        }

        /* @var Answer[] $answers */
        $answers = Answer::find()->all();

        foreach ($answers as $answer) {
            $answer->question_id = null;
            $answer->save(false);
        }
        Question::deleteAll();
        Answer::deleteAll();
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
