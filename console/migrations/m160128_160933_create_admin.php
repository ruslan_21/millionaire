<?php

use yii\db\Schema;
use yii\db\Migration;

class m160128_160933_create_admin extends Migration
{
    public function up()
    {
        $this->insert('user', [
            'username' => 'admin',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'auth_key' => Yii::$app->security->generateRandomString(),
            'status' => \common\models\User::STATUS_ACTIVE,
            'created_at' => time(),

        ]);
    }

    public function down()
    {
        $this->delete('user', ['username'=> 'admin']);
    }
}
